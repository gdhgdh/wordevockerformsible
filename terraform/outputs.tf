output "elbhostname" {
  value = "${aws_elb.wordpress_elb.dns_name}"
}

output "rdshostname" {
  value = "${module.wordpress_rds_instance.rds_instance_address}"
}

output "rdspassword" {
  value = "${var.rds_password}"
}
