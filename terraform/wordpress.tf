resource "aws_elb" "wordpress_elb" {
  name = "gavin-${var.envname}-wordpress-elb"

  security_groups = ["${module.elb_wordpress_sg.security_group_id_web}"]
  subnets         = ["${split(",", module.vpc.public_subnets)}"]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/wp-admin/install.php"
    interval            = 5
  }
}

module "wordpress_asg" {
  source = "./modules/tf_aws_asg_elb"

  rolename             = "wordpress"
  ami_id               = "${var.wordpress_ami_id}"
  instance_type        = "${var.wordpress_instance_type}"
  iam_instance_profile = ""
  key_name             = "${var.keypair}"
  security_groups      = "${module.wordpress_sg.security_group_id_web},${module.wordpress_ssh_sg.security_group_id_ssh_sgid}"

  asg_name                        = "gavin-${var.envname}-wordpress"
  asg_number_of_instances         = "${var.wordpress_min_instances}"
  asg_minimum_number_of_instances = "${var.wordpress_max_instances}"
  load_balancer_names             = "${aws_elb.wordpress_elb.name}"
  user_data                       = "bootstrap.sh"
  health_check_type               = "EC2"
  availability_zones              = "${lookup(var.aws_availability_zones, var.aws_region)}"
  vpc_zone_subnets                = "${module.vpc.public_subnets}"
}
