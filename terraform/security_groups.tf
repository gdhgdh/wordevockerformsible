module "elb_wordpress_sg" {
  source              = "./modules/tf_aws_sg/sg_web_cidr"
  vpc_id              = "${module.vpc.vpc_id}"
  security_group_name = "${var.envname} Wordpress ELB"
}

module "wordpress_sg" {
  source              = "./modules/tf_aws_sg/sg_web_sgid"
  vpc_id              = "${module.vpc.vpc_id}"
  security_group_name = "${var.envname} Wordpress EC2"
  source_sgid         = "${module.elb_wordpress_sg.security_group_id_web}"
}

module "wordpress_ssh_sg" {
  source              = "./modules/tf_aws_sg/sg_ssh_cidr"
  vpc_id              = "${module.vpc.vpc_id}"
  security_group_name = "${var.envname} Wordpress EC2 Admin access"
  source_cidr_block   = "${var.admin_cidr}"
}

module "rds_sg" {
  source              = "./modules/tf_aws_sg/sg_mysql_sgid"
  vpc_id              = "${module.vpc.vpc_id}"
  security_group_name = "${var.envname} RDS access"
  source_sgid         = "${module.wordpress_sg.security_group_id_web}"
}
