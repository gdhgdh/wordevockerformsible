module "vpc" {
  source = "./modules/tf_aws_vpc"

  name = "${var.envname}-wordpress"

  cidr            = "${var.vpc_cidr}"
  public_subnets  = "${var.vpc_public_cidrs}"
  private_subnets = "${var.vpc_private_cidrs}"
  azs             = "${lookup(var.aws_availability_zones, var.aws_region)}"

  enable_dns_support = true
}
