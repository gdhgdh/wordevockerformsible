module "wordpress_rds_instance" {
  source = "./modules/tf_aws_rds"

  //RDS Instance Inputs
  rds_instance_name     = "gavin-${var.envname}-wordpress-rds"
  rds_allocated_storage = 10
  rds_engine_type       = "mysql"
  rds_engine_version    = "5.6.29"
  rds_instance_class    = "db.t2.micro"
  database_name         = "wordpress"
  database_user         = "root"
  database_password     = "${var.rds_password}"
  rds_security_group_id = "${module.rds_sg.security_group_id_mysql}"
  db_parameter_group    = ""

  // DB Subnet Group Inputs
  rds_subnet_ids = "${module.vpc.private_subnets}"
}
