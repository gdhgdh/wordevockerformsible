variable "rds_password" {
  default = "Sd2kf214^12vSdvo1_vm"
}

variable "admin_cidr" {
  default = "80.192.142.9/32"
}

provider "aws" {
  region = "${var.aws_region}"
}

variable "aws_availability_zones" {
  default = {
    eu-west-1 = "eu-west-1a,eu-west-1b,eu-west-1c"
  }
}

variable "envname" {}

variable "aws_region" {
  default = "eu-west-1"
}

variable "aws_vpc_id" {}

variable "vpc_cidr" {}

variable "vpc_public_cidrs" {}

variable "vpc_private_cidrs" {}

variable "wordpress_min_instances" {}

variable "wordpress_max_instances" {}

variable "wordpress_ami_id" {}

variable "wordpress_instance_type" {}

variable "keypair" {}
