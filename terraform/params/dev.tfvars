envname="dev"

aws_vpc_id="vpc-7d25e219"

vpc_cidr="10.150.0.0/21"
vpc_public_cidrs="10.150.0.0/24,10.150.1.0/24,10.150.2.0/24"
vpc_private_cidrs="10.150.3.0/24,10.150.4.0/24,10.150.5.0/24"

rds_instance_type="db.t2.micro"
rds_disk_size="10"

keypair="caketest"
wordpress_ami_id="ami-f95ef58a"
wordpress_instance_type="t2.micro"

wordpress_min_instances="2"
wordpress_max_instances="2"
