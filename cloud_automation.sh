#!/bin/bash -e

if [ $# -ne 4 ]; then
  echo "Usage: $0 <app> <environment> <num_servers> <server_size>"
  exit 1
fi

valid_apps="hello_world"
valid_envs="dev"
valid_instance_types="t2.micro t2.small t2.medium"

function checklist () {
  # $1 = describe the kind of thing we are checking
  # $2 = the value to check
  # $3 -> $n = all the remaining values to check against

  local thingtype=$1
  local thingvalue=$2
  local valuetest
  local ret

  shift 2 # Swallow the first two values since we've already read them

  for valuetest in $@; do
    if [ "${thingvalue}" == "${valuetest}" ]; then
      ret=${thingvalue}
    fi
  done

  if [ "${ret}" == "" ]; then
    echo "I don't know about ${thingtype} '${thingvalue}'." >/dev/stderr
    echo "I know about: $@" >/dev/stderr
    exit 1
  fi

  echo ${ret}

}

appname=$(checklist application $1 $valid_apps)
envname=$(checklist environment $2 $valid_envs)
asgsize=$(checklist "auto-scaling group size" $3 `seq 1 4`)
instance_type=$(checklist "instance size" $4 $valid_instance_types)

echo "Deploying ${appname} to the ${envname} environment using ${instance_type} instances"
echo "Auto-scaling group size is ${asgsize}"

# Now we have values we can trust, start the actual work.

cd terraform && make apply INSTANCE_TYPE=${instance_type} ASG_SIZE=${asgsize}

elbhostname=$(terraform output elbhostname)
rdshostname=$(terraform output rdshostname)
rdspassword=$(terraform output rdspassword)

cd ..

echo
echo " Now running Ansible to configure the Wordpress container on the EC2 instance(s)"
echo

cd ansible && ansible-playbook --extra-vars "{\"rds_hostname\": \"$rdshostname\",\"rds_password\": \"$rdspassword\", \"elb_hostname\": \"$elbhostname\" }" blog.yml

echo
echo "${appname} WordPress URL: http://${elbhostname}/"
echo

cd ..

# todo: nginx frontend, fpm backend
# todo: check out the github application repo and sync it to /var/www/html in the container via a host -> container volume
# todo rds data restore?
