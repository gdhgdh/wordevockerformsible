#!/bin/bash -e

echo
echo "Restoring Wordpress SQL database..."
echo

rdshostname=$(cd terraform && terraform output rdshostname && cd ..)
rdspassword=$(cd terraform && terraform output rdspassword && cd ..)

cd ansible && ansible-playbook --extra-vars "{\"rds_hostname\": \"$rdshostname\",\"rds_password\":\"$rdspassword\"}" restore.yml

