# Intro

c_a^k+e's tech test. bash-driven Terraform deploys EC2 workers running Dockerised Wordpress configured using Ansible, all behind an ELB

# Setup

Assumes a client workstation using Ubuntu 16.04 LTS

- Terraform 0.6.16 or newer

- Ansible 2.1 or newer (package name `ansible` from Ansible's own PPA [[1]])
- `awscli`
- `python-boto`
- IAM user details for the target AWS account in `~/.aws/credentials` - verify with `aws iam get-user`
- PEM shared key loaded into the SSH agent: `ssh-add /path/to/caketest.pem`
- Edit the top of `terraform/vars.tf` with the CIDR block used by your Internet connection. This will set the SSH security group rules on the instances themselves.

# Make it go
- `./cloud_automation hello_world dev 2 t2.micro`

On the *FIRST* run, this will produce some ugly-looking errors which are safe to ignore because there is no active database yet, and the tasks require a fully-working WP installation.

Here are the tasks which will generate error messages:

```
TASK [wordpress : set wp home to current ELB hostname] *************************
fatal: [x.x.x.x]: FAILED! => {"changed...
...ignoring 
fatal: [x.x.x.x]: FAILED! => {"changed...
...ignoring 

TASK [wordpress : set wp siteurl to current ELB hostname] **********************
fatal: [x.x.x.x]: FAILED! => {"changed...
...ignoring 
fatal: [x.x.x.x]: FAILED! => {"changed...
...ignoring 
```

- Visit the URL shown at `hello_world WordPress URL:` and complete the WordPress '5 minute install' 
- Re-run the same `cloud_automation.sh` command to verify that everything executes cleanly
- There is a plugin (Really Simple CAPTCHA) and a theme (QuickChic) already available above and beyond the core WordPress installation because of the `code/` directory. Try and activate them.

# Database actions

The scripts `backupdb.sh` and `restoredb.sh` are convenience scripts to backup and restore the remote RDS `wordpress` database. The backup is stored in the `backups` directory and will be overwritten by any future backup.

[1]: http://docs.ansible.com/ansible/intro_installation.html#latest-releases-via-apt-ubuntu
